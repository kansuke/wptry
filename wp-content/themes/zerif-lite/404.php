<?php

/**

 * The template for displaying 404 pages (Not Found).

 *

 * @package zerif

 */



get_header(); ?>

<div class="clear"></div>

</header> <!-- / END HOME SECTION  -->



<div id="content" class="site-content">

<div class="container">



<div class="content-left-wrap col-md-9">

	<div id="primary" class="content-area">

		<main id="main" class="site-main" role="main">



		<article>

			<header class="entry-header">

				<h1 class="entry-title"><?php _e( '404 - Page introuvable', 'zerif-lite' ); ?></h1>

			</header><!-- .entry-header -->



			<div class="entry-content">

				<p><?php _e( 'Il semble que la page demand&eacute;e n&#39;existe pas. ', 'sensible-wp' ); ?><a href="<?php echo $_SERVER['HTTP_REFERER']; ?>"><?php _e('Retourner sur la page pr&eacute;c&eacute;dente.', 'zerif-lite'); ?></a></p>

				

			</div><!-- .entry-content -->

		</article><!-- #post-## -->



		</main><!-- #main -->

	</div><!-- #primary -->

</div>



<div class="sidebar-wrap col-md-3 content-left-wrap">

	<?php get_sidebar(); ?>

</div><!-- .sidebar-wrap -->



</div>



<?php get_footer(); ?>